package postfix

import (
	"strings"
	"time"
)

type Smtpd struct {
	Used    bool      `json:"used" xml:"used"`
	Client  string    `json:"client" xml:"client"`
	Created time.Time `json:"created" xml:"created"`
}

func (s *Smtpd) set(msg string) error {
	s.Used = true
	arr := strings.Split(msg, "=")
	s.Client = arr[1]
	s.Created = time.Now()
	return nil
}
