package postfix

import (
	"errors"
	"strconv"
	"strings"
	"time"
)

type Qmgr struct {
	From    string    `json:"from" xml:"from"`
	Size    int       `json:"size" xml:"size"`
	Nrcpt   int       `json:"nrcpt" xml:"nrcpt"`
	Message string    `json:"message" xml:"message"`
	Status  string    `json:"status" xml:"status"`
	Created time.Time `json:"created" xml:"created"`
	Smtp    []Smtp    `json:"smtp" xml:"smtps>smtp"`
}

func (q *Qmgr) set(msg string) error {
	if msg == "removed" {
		q.Status = "removed"
		return errors.New("removed")
	}
	arr := strings.Split(msg, " (")
	if len(arr) == 2 {
		q.Message = strings.TrimRight(arr[1], ")")
	}
	arr = strings.Split(arr[0], ", ")
	for _, v := range arr {
		val := strings.Split(v, "=")
		switch val[0] {
		case "from":
			q.From = val[1]
		case "nrcpt":
			q.Nrcpt, _ = strconv.Atoi(val[1])
		case "size":
			q.Size, _ = strconv.Atoi(val[1])
		case "status":
			q.Status = val[1]
		}
	}
	if q.Status == "" {
		q.Status = "sent"
	}
	q.Created = time.Now()
	return nil
}
