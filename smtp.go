package postfix

import (
	"errors"
	"strconv"
	"strings"
	"time"
)

type Smtp struct {
	To      string    `json:"to" xml:"to"`
	Relay   string    `json:"relay" xml:"relay"`
	Delay   float32   `json:"delay" xml:"delay"`
	Delays  []float32 `json:"delays" xml:"delays"`
	Dsn     string    `json:"dsn" xml:"dsn"`
	Status  string    `json:"status" xml:"status"`
	Message string    `json:"message" xml:"message"`
	Created time.Time `json:"created" xml:"created"`
}

func (s *Smtp) set(msg string) error {
	//log.Println(msg)
	arr := strings.Split(msg, " (")
	if len(arr)<2 {
		return errors.New("не понятное сообщение SMTP")
	}
	s.Message = strings.TrimRight(arr[1], ")")
	arr = strings.Split(arr[0], ", ")
	if len(arr)<6 {
		return errors.New("не понятное сообщение SMTP 2")
	}
	for _, v := range arr {
		val := strings.Split(v, "=")
		if len(arr)<2 {
			return errors.New("не понятное сообщение SMTP 3")
		}
		switch val[0] {
		case "delay":
			value, _ := strconv.ParseFloat(val[1], 32)
			s.Delay = float32(value)
		case "delays":
			//arr_delays := strings.Split(val[1], "/")
			for _, v_delay := range strings.Split(val[1], "/") {
				value, _ := strconv.ParseFloat(v_delay, 32)
				s.Delays = append(s.Delays, float32(value))
			}
		case "to":
			s.To = val[1]
		case "relay":
			s.Relay = val[1]
		case "dsn":
			s.Dsn = val[1]
		case "status":
			s.Status = val[1]

		}
	}
	s.Created = time.Now()
	return nil
}
