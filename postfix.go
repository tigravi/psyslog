package postfix

import (
	"encoding/json"
	"errors"
	"strings"
	"time"
)

type PSyslog struct {
	Server   Server   `json:"server" xml:"server"`
	Pickup   Pickup   `json:"pickup" xml:"pickup"`
	Smtpd    Smtpd    `json:"smtpd" xml:"smtpd"`
	Cleanup  Cleanup  `json:"cleanup" xml:"cleanup"`
	Opendkim Opendkim `json:"opendkim" xml:"opendkim"`
	Qmgr     []Qmgr   `json:"qmgr" xml:"qmgrs>qmgr"`
	Bounce   Bounce   `json:"bounce" xml:"bounce"`
	Logs     []string `json:"logs" xml:"logs>log"`
	msgTopic string
}

func (p *PSyslog) GetMsgTopic() string {
	return p.msgTopic
}
func (s *PSyslog) Set(tag string, hostname string, client string, content string) {
	msg, err := s.Server.set(hostname, client, content)
	s.msgTopic = ""
	s.Logs = append(s.Logs, msg)
	if err != nil {
		return
	}
	switch tag {
	case "postfix/pickup":
		s.Pickup.set(msg)
		s.msgTopic = "pickup"
	case "postfix/smtpd":
		s.Smtpd.set(msg)
		s.msgTopic = "smtpd"
	case "postfix/cleanup":
		s.Cleanup.set(msg)
		s.msgTopic = "cleanup"
	case "postfix/bounce":
		s.Bounce.set(msg)
		s.msgTopic = "bounce"

	case "postfix/qmgr":
		var q Qmgr
		err := q.set(msg)
		s.msgTopic = "qmgr"
		if err == nil {
			s.Qmgr = append(s.Qmgr, q)
			s.msgTopic += ":"+q.Status
		} else {
			if err.Error() == "removed" {
				//log.Println("key:", s.Server.Client+":"+s.Server.Uuid)
				s.Server.Deleted = time.Now()
			}
			if err.Error() == "expired" {
				//s.Server.Deleted = time.Now()
			}
			s.msgTopic += ":" + err.Error()

		}
	case "postfix/smtp":
		var sm Smtp
		err := sm.set(msg)

		if err == nil {
			l := len(s.Qmgr) - 1
			if l >= 0 {
				s.Qmgr[l].Smtp = append(s.Qmgr[l].Smtp, sm)
				s.msgTopic += "smtp:" + sm.Status

			}
			//s.Server.Status = s.Forwardings[len(s.Forwardings)-1].Smtp.Status
		}

	case "opendkim":
		s.Opendkim.set(msg)
	}
}

// возвращает UUID письма и остальное тело сообщения. если UUID не выделен то 0 элемент массива будет пустым
func ContentParsUuid(content string) ([]string, error) {
	arr := strings.Split(content, ": ")
	//len(arr[0]) != 10 ||
	switch arr[0] {
	case "NOQUEUE":
		return []string{"", content}, errors.New("Нет UUID письма")
	}
	if arr[0] != strings.ToUpper(arr[0]) {
		return []string{"", content}, errors.New("Нет UUID письма")
	}
	return []string{arr[0], strings.TrimLeft(content, arr[0]+":")[1:]}, nil
}

func (s *PSyslog) GetStringJson() string {
	buf, _ := json.Marshal(s)
	return string(buf)
}

func (s *PSyslog) SetStringJson(jString string) error {
	err := json.Unmarshal([]byte(jString), s)
	return err
}
