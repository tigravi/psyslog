package postfix

import (
	"strings"
	"time"
)

type Server struct {
	Hostname string    `json:"hostname" xml:"hostname"`
	Client   string    `json:"client" xml:"client"`
	Uuid     string    `json:"uuid" xml:"uuid"`
	Created  time.Time `json:"created" xml:"created"`
	Deleted  time.Time `json:"deleted" xml:"deleted"`
	removed  bool      `json:"removed" xml:"removed"`
}

func (s *Server) set(hostname string, client string, content string) (string, error) {
	arr, err := ContentParsUuid(content)
	if err != nil {
		return "", err
	}
	if s.Uuid == "" && s.Uuid == strings.ToUpper(s.Uuid) {
		s.Hostname = hostname
		s.Client = client
		s.Uuid = arr[0]
		s.Client = strings.Split(client, ":")[0]
		s.Created = time.Now()
	}
	return arr[1], nil
}
