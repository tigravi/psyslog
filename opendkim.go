package postfix

import (
	"strings"
	"time"
)

type Opendkim struct {
	Used     bool      `json:"used" xml:"used"`
	Selector string    `json:"selector" xml:"selector"`
	Domain   string    `json:"domain" xml:"domain"`
	Message  string    `json:"message" xml:"message"`
	Created  time.Time `json:"created" xml:"created"`
}

func (o *Opendkim) set(msg string) error {
	arr := strings.Split(msg, " (")
	if len(arr) != 2 {
		o.Message = arr[0]
		return nil
	}
	o.Message = arr[0]

	arr = strings.Split(strings.ReplaceAll(arr[1], ")", ""), ", ")
	for _, v := range arr {
		val := strings.Split(v, "=")
		switch val[0] {
		case "s":
			o.Selector = val[1]
		case "d":
			o.Domain = val[1]
		}
	}
	if o.Selector != "" {
		o.Used = true
	}
	o.Created = time.Now()
	return nil
}
