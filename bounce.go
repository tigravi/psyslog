package postfix

import (
	"strings"
	"time"
)

type Bounce struct {
	Used    bool      `json:"used" xml:"used"`
	Uuid    string    `json:"uuid" xml:"uuid"`
	Message string    `json:"message" xml:"message"`
	Created time.Time `json:"created" xml:"created"`
}

func (b *Bounce) set(msg string) error {
	arr := strings.Split(msg, ": ")
	b.Used = true
	b.Uuid = arr[1]
	b.Message = arr[0]
	b.Created = time.Now()
	return nil
}
