package postfix

import (
	"strings"
	"time"
)

type Cleanup struct {
	MessageId string    `json:"messageId" xml:"messageId"`
	Created   time.Time `json:"created" xml:"created"`
}

func (s *Cleanup) set(msg string) {
	s.MessageId = strings.ReplaceAll(msg, "message-id=", "")
	s.Created = time.Now()
}
