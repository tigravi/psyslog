package postfix

import (
	"strconv"
	"strings"
	"time"
)

type Pickup struct {
	Used    bool      `json:"used" xml:"used"`
	Uid     int       `json:"uid" xml:"uid"`
	From    string    `json:"from" xml:"from"`
	Created time.Time `json:"created" xml:"created"`
}

func (p *Pickup) set(msg string) error {
	p.Used = true
	arr := strings.Split(msg, " ")
	for _, v := range arr {
		val := strings.Split(v, "=")
		switch val[0] {
		case "uid":
			p.Uid, _ = strconv.Atoi(val[1])
		case "from":
			p.From = val[1]
		}
	}
	p.Created = time.Now()
	return nil
}
